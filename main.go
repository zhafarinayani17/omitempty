package main

import (
	"encoding/json"
	"fmt"
)

// //tanpa Omitempty
// type Dog struct {
// 	Breed    string
// 	WeightKg int
// }

// func main() {
// 	d := Dog{
// 		Breed: "dalmation",
// 	}
// 	b, _ := json.Marshal(d)
// 	fmt.Println(string(b))
// }

//dengan omitempety
type Dog struct {
	Breed    string
	WeightKg int
	Size     *dimension `json:", omitempty"`
}

type dimension struct {
	Height int
	Width  int
}

func main() {
	a := &dimension{
		Height: 17,
		Width:  28,
	}
	d := Dog{
		Breed: "dalmation",
		Size:  a,
	}

	b, _ := json.Marshal(d)
	fmt.Println(string(b))
}

// type Restaurant struct {
// 	NumberOfCustomers int `json:",omitempty"`
// }

// func main() {
// 	// d1 := Restaurant{}
// 	// b, _ := json.Marshal(d1)
// 	// fmt.Println(string(b))
// 	// //Prints: {}

// }

// type Restaurant struct {
// 	NumberOfCustomers *int `json:",omitempty"`
// }

// func main() {
// 	d1 := Restaurant{}
// 	b, _ := json.Marshal(d1)
// 	fmt.Println(string(b))
// 	//Prints: {}

// 	n := 0
// 	d2 := Restaurant{
// 		NumberOfCustomers: &n,
// 	}
// 	b, _ = json.Marshal(d2)
// 	fmt.Println(string(b))
// 	//Prints: {"NumberOfCustomers":0}
// }
